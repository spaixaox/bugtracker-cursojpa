package br.com.triadworks.bugtracker.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Usuario;

public class UsuarioDao {
   
	private EntityManager manager;

	public UsuarioDao(EntityManager manager) {
		this.manager = manager;
	}
	
	public Usuario busca(Integer id){
		return this.manager.find(Usuario.class, id);
	}
	
	public List<Usuario> lista(){
		return this.manager.createQuery("select u from Usuario u", Usuario.class)
				.getResultList();
	}
	
	public void atualiza(Usuario usuario){
		this.manager.merge(usuario);
	}
	
	public void adiciona(Usuario usuario){
		this.manager.persist(usuario);
	}
	
	public void remover(Usuario usuario){
		this.manager.remove(usuario);
	}	
}
