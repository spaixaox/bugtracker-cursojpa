package br.com.triadworks.bugtracker.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Bug;

public class BugDao {
   
	private EntityManager manager;

	public BugDao(EntityManager manager) {
		this.manager = manager;
	}
	
	public Bug busca(Integer id){
		return this.manager.find(Bug.class, id);
	}
	
	public List<Bug> lista(){
		return this.manager.createQuery("select u from Bug u", Bug.class)
				.getResultList();
	}
	
	public void atualiza(Bug bug){
		this.manager.merge(bug);
	}
	
	public void adiciona(Bug bug){
		this.manager.persist(bug);
	}
	
	public void remover(Bug bug){
		this.manager.remove(bug);
	}	
}
