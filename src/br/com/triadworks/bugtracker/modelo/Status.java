package br.com.triadworks.bugtracker.modelo;

public enum Status {
   ABERT0("Aberto"),
   FECHADO("Fechado");
   
   private String descricao;
	
   Status(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}   
}
