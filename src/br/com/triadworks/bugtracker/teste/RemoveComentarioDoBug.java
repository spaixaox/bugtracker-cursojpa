package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.BugDao;
import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.modelo.Comentario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class RemoveComentarioDoBug {

	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		
		Comentario comentario = new Comentario();
		comentario.setId(1);

		BugDao dao = new BugDao(manager);
		
		Bug bug = dao.busca(1);
		
		boolean removido = bug.getComentarios().remove(comentario);
		
		dao.atualiza(bug);
		
		manager.getTransaction().commit();
		manager.close();
		
		if (removido){
			System.out.println("Comentário Removido com sucesso!");
		} else {
			System.out.println("Comentário não foi Removido com sucesso!");
		}
	}
	
}
