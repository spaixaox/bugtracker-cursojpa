package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AdicaoDeUsuario_Old {

	public static void main(String[] args) {
		


		
		/*
		EntityManagerFactory factory = Persistence
				                       .createEntityManagerFactory("bugtracker");
	    long tempoInicial = System.currentTimeMillis();	
	    
		EntityManager manager = factory.createEntityManager();
		*/
		
	    long tempoInicial = System.currentTimeMillis();	
	    
	    
		EntityManager manager = new JPAUtil().getEntityManager();

		Usuario usuario = new Usuario();
		usuario.setLogin("spaixaox");
		usuario.setNome("Sandro Paixão");
		usuario.setSenha("123");
		usuario.setEmail("spaixaox@gmail.com");
		
		try {
			manager.getTransaction().begin();
			manager.persist(usuario);
			manager.getTransaction().commit();
			
			System.out.println("Usuário adicionado com sucesso");
			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			System.out.println("Erro ao adicionar usuário: "+e.getMessage());
		} finally {
			manager.close();			

			long tempoFinal = System.currentTimeMillis();		
  		    System.out.printf("%.3f ms%n", (tempoFinal - tempoInicial) / 1000d);			
		}


	}

}
