package br.com.triadworks.bugtracker.teste;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.modelo.Status;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AdicaoDeBug {

	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Bug bug = new Bug();
		bug.setSumario("Bug 1 no IE");
		bug.setDescricao("Bug ao abrir página de cadastro de usuários");
		bug.setStatus(Status.ABERT0);
		bug.setCriadoEm(new Date());
		
		manager.getTransaction().begin();
		
		manager.persist(bug);
		
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println("Bug adicionado com sucesso!");
	}
	
}
