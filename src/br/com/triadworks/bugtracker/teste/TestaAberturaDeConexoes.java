package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.util.JPAUtil;

public class TestaAberturaDeConexoes {

	public static void main(String[] args) throws InterruptedException {
		
		// Tentamos abrir 30 conexões
		for (int i=0; i < 30; i++){
			EntityManager manager = new JPAUtil().getEntityManager();
			manager.getTransaction().begin();
			System.out.println("Criado EntityManager número "+i);			
		}
		
		Thread.sleep(30 * 1000);

	}
	
}
