package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.UsuarioDao;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class DelecaoDeUsuario {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        UsuarioDao dao = new UsuarioDao(manager);	    
		
		try {

			manager.getTransaction().begin();
			
			Usuario usuario = dao.busca(1);
			dao.remover(usuario);
			
			manager.getTransaction().commit();		
			System.out.println("Usuário removido com sucesso");			
		}  catch (Exception e) {

			manager.getTransaction().rollback();
			System.out.println("Erro ao tentar remover usuario :"+e.getMessage());			
		}
		finally {
			manager.close();			
		}


	}

}
