package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.BugDao;
import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class BuscaUsuarioDoBug {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        BugDao dao = new BugDao(manager);	 
        
		try {

	        manager.getTransaction().begin();
	        
			Bug bug = dao.busca(4);
	        
	        bug.getResponsavel().setNome("Sandro Paixão 2");
	        
	        manager.getTransaction().commit();
	        
    		System.out.println("Responsável: "+bug.getResponsavel().getNome());

		} finally {
			manager.close();			
		}

	}
}
