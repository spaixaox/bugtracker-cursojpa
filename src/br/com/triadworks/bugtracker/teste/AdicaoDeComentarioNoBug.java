package br.com.triadworks.bugtracker.teste;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.BugDao;
import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.modelo.Comentario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AdicaoDeComentarioNoBug {

	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		
		Comentario novoComentario = new Comentario();
		novoComentario.setDescricao("Pesquisa continua muito lenta no PostgreSql!");
		novoComentario.setCriadoEm(new Date());

		BugDao dao = new BugDao(manager);
		
		Bug bug = dao.busca(1);
		bug.getComentarios().add(novoComentario);

		dao.atualiza(bug);
		
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println("Comentário adiciona ao Bug com sucesso!");
	}
	
}
