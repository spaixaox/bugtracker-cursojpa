package br.com.triadworks.bugtracker.teste;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.UsuarioDao;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class ListaUsuarios {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        UsuarioDao dao = new UsuarioDao(manager);	    
		
		try {

			List<Usuario> lista = dao.lista();
			
			for(Usuario usuario: lista){
				System.out.println("Id: "+usuario.getId()+" Nome: "+usuario.getNome());
			}

		} finally {
			manager.close();			
		}


	}

}
