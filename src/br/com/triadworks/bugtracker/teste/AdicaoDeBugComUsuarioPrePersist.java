package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.modelo.Status;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AdicaoDeBugComUsuarioPrePersist {

	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Usuario usuario = manager.find(Usuario.class, 2);
		
		Bug bug = new Bug();
		bug.setResponsavel(usuario);
		bug.setSumario("Bug ao sair da aplicação");
		bug.setDescricao("Não está chamando a tela de login");
		bug.setStatus(Status.ABERT0);
		//bug.setCriadoEm(new Date()); será setado pelo @PrePersist da classe Bug
		
		manager.getTransaction().begin();
		
		manager.persist(bug);
		
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println("Bug adicionado com sucesso!");
	}
	
}
