package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.UsuarioDao;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AtualizacaoDeUsuario {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        UsuarioDao dao = new UsuarioDao(manager);	    
		
		try {

			manager.getTransaction().begin();
			
			Usuario usuario = dao.busca(9);
			usuario.setNome("Luiz Xavier 2");
			//dao.atualiza(usuario);
			
			manager.getTransaction().commit();		
			System.out.println("Usuário atualizado com sucesso");			
		}  catch (Exception e) {

			manager.getTransaction().rollback();
			System.out.println("Erro ao tentar remover usuario :"+e.getMessage());			
		}
		finally {
			manager.close();			
		}


	}

}
