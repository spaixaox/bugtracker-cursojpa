package br.com.triadworks.bugtracker.teste;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.modelo.Bug;
import br.com.triadworks.bugtracker.modelo.Status;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class AdicaoDeBugComUsuarioComErro {

	public static void main(String[] args) {
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Usuario usuario = new Usuario();
	    usuario.setLogin("zemaria");
		usuario.setSenha("123");
		usuario.setNome("Zé Maria");
		usuario.setEmail("zemaria@gmail.com");
		
		Bug bug = new Bug();
		bug.setResponsavel(usuario);
		bug.setSumario("Bug ao logar na aplicação");
		bug.setDescricao("Está retornando erro de página não encontrada");
		bug.setStatus(Status.ABERT0);
		bug.setCriadoEm(new Date());
		
		manager.getTransaction().begin();
		
		manager.persist(bug);
		
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println("Bug adicionado com sucesso!");
	}
	
}
