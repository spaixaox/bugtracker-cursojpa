package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.UsuarioDao;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class BuscaUsuarioPorId {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        UsuarioDao dao = new UsuarioDao(manager);	    
		
		try {

			Usuario usuario = dao.busca(5);
			
    		System.out.println(usuario.getNome());

		} finally {
			manager.close();			
		}

	}
}
