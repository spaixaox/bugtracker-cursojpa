package br.com.triadworks.bugtracker.teste;

import javax.persistence.EntityManager;

import br.com.triadworks.bugtracker.dao.UsuarioDao;
import br.com.triadworks.bugtracker.modelo.Usuario;
import br.com.triadworks.bugtracker.util.JPAUtil;

public class ForcaSincronizacaoDoEntityManager {

	public static void main(String[] args) {

    
	    EntityManager manager = new JPAUtil().getEntityManager();
        UsuarioDao dao = new UsuarioDao(manager);
        
   
		
		try {
			
	        manager.getTransaction().begin();

	        Usuario usuario = dao.busca(5);
	        usuario.setEmail("fulano@mail.com");
	        
	        System.out.println("Antes do flush!");
	        
	        manager.flush();
	        
	        System.out.println("Depois do flush!");   
	        
	        manager.getTransaction().commit();
			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			System.out.println("Erro  "+e.getMessage());
		} finally {
			manager.close();			
		}


	}

}
